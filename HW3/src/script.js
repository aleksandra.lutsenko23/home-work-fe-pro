'use strict';

let styles = ['Jazz', 'Blues'];

// add to the end of the array

styles.push('Rock-n-roll');
console.log(styles);

// replace item in the middle

function findMiddle(arr) {
  const middle = Math.round((arr.length - 1) / 2);
  styles.splice(middle, 1, 'Classic');
  console.log(styles);
}
findMiddle(styles);

// delete thr first el from the array

const deleted = styles.shift();
console.log(deleted);

//input items to the begenning og the array

styles.unshift('Rap', 'Reggie');
console.log(styles);
 

