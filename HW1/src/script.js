"use strict";

let x = 6;
let y = 14;
let z = 4;

x += y - x++ * z; //посфиксный первый по приоритету но операция выполняеться после поэтому 6 * 4 = 24, 20 - 24 = -4,
//последний по приоритетности минус поэтому 21-24 = -4

console.log(x);

x = 6;
y = 14;
z = 4;

z = --x - y * 5; // префиксный дикримент самый приоритетный поэтому х - 1 = 5, потом умножение - 14 * 5 = 70 и последнее по приоритетности отнимание 5 - 70 = -65;

console.log(z);

x = 6;
y = 14;
z = 4;

y /= x + (5 % z); // самый высокий приорите у скобок поэтому несмотря на низкий приоритет операторы, выполняем сначала остаток и получаем 1, потом 6 + 1 = 7 и 14 / 7 = 2

console.log(y);

x = 6;
y = 14;
z = 4;

console.log(z - x++ + y * 5); // пjcabrcysq инкремент будет с тем же значением - 6, 4 - 6 = -2  и потом умножение 14 * 5 = 70, -2 + 70 дает 68

x = 6;
y = 14;
z = 4;

x = y - x++ * z; //префиксный инкремент, но после поэтому 6 * 4 = 24б потом отнимание 14 - 24 = -10

console.log(x);

// Class work

const userName = prompt("Введите ваше имя");
const userSurname = prompt("Введите вашу фамилию");
const userBirthday = prompt("Введите год рождения");

const firstNumber = prompt("Введите первое число");
const secondNumber = prompt("Введите второе число");
const thirdNumber = prompt("Введите третье число");

const firstNumberOutput = parseInt(firstNumber);
const secondNumberOutput = parseInt(secondNumber);
const thirdNumberOutput = parseInt(thirdNumber);

const totalNumber =
  (firstNumberOutput + secondNumberOutput + thirdNumberOutput) / 3;

document.write("Имя - " + userName + "<br>");
document.write("Фамилия - " + userSurname + "<br>");
document.write("Год рождения - " + userBirthday + "<br>");
document.write("Среднее арифметическое 3 чисел - " + totalNumber + "<br>");
